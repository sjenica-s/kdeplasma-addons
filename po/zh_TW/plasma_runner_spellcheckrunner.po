# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2008, 2009.
# Franklin Weng <franklin@mail.everfocus.com.tw>, 2010.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: krunner_spellcheckrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-23 00:46+0000\n"
"PO-Revision-Date: 2018-09-16 22:15+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: spellcheck.cpp:98 spellcheck_config.cpp:65 spellcheck_config.cpp:96
#, kde-format
msgid "spell"
msgstr "拼字"

#: spellcheck.cpp:103
#, kde-format
msgctxt ""
"Spelling checking runner syntax, first word is trigger word, e.g.  \"spell\"."
msgid "%1:q:"
msgstr "%1:q:"

#: spellcheck.cpp:104
#, kde-format
msgid "Checks the spelling of :q:."
msgstr "檢查 :q: 的拼字。"

#: spellcheck.cpp:224
#, kde-format
msgctxt "Term is spelled correctly"
msgid "Correct"
msgstr "修正"

#: spellcheck.cpp:233
#, kde-format
msgid "Suggested term"
msgstr "建議術語"

#: spellcheck.cpp:263
#, kde-format
msgid "No dictionary found, please install hspell"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: spellcheck_config.ui:17
#, kde-format
msgid "Spell Check Settings"
msgstr "拼字檢查設定"

#. i18n: ectx: property (text), widget (QCheckBox, m_requireTriggerWord)
#: spellcheck_config.ui:23
#, kde-format
msgid "&Require trigger word"
msgstr "需要觸發單字(&R)"

#. i18n: ectx: property (text), widget (QLabel, label)
#: spellcheck_config.ui:32
#, kde-format
msgid "&Trigger word:"
msgstr "觸發單字(&T)："

#. i18n: ectx: property (text), widget (QPushButton, m_openKcmButton)
#: spellcheck_config.ui:62
#, fuzzy, kde-format
#| msgid "Configure Dictionaries..."
msgid "Configure Dictionaries…"
msgstr "設定字典…"

#~ msgctxt "@action"
#~ msgid "Copy to Clipboard"
#~ msgstr "複製至剪貼板"

#~ msgid "Could not find a dictionary."
#~ msgstr "找不到字典。"
