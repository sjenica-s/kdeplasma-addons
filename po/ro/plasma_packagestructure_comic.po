# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2010-07-17 19:32+0300\n"
"Last-Translator: Sergiu Bivol <sergiu@ase.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "Imagini"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "Scripturi executabile"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "Fișier-script principal"
