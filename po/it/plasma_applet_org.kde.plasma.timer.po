# translation of plasma_applet_timer.po to Italian
# Copyright (C) 2008 Free Software Foundation, Inc.
# Vincenzo Reale <smart2128vr@gmail.com>, 2008, 2012, 2014, 2018, 2019, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_timer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-01 00:49+0000\n"
"PO-Revision-Date: 2022-08-07 21:05+0200\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Aspetto"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Predefined Timers"
msgstr "Timer predefiniti"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr "Avanzate"

#: package/contents/ui/CompactRepresentation.qml:133
#, kde-format
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "Sospendi timer"

#: package/contents/ui/CompactRepresentation.qml:133
#, kde-format
msgctxt "@action:button"
msgid "Start Timer"
msgstr "Avvia timer"

#: package/contents/ui/CompactRepresentation.qml:189
#: package/contents/ui/CompactRepresentation.qml:206
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] "%1s"
msgstr[1] "%1s"

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr "Alla fine del tempo:"

#: package/contents/ui/configAdvanced.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Execute command:"
msgstr "Esegui comando:"

#: package/contents/ui/configAppearance.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr "Visualizza:"

#: package/contents/ui/configAppearance.qml:33
#, kde-format
msgctxt "@option:check"
msgid "Show title:"
msgstr "Mostra titolo:"

#: package/contents/ui/configAppearance.qml:50
#, kde-format
msgctxt "@option:check"
msgid "Show seconds"
msgstr "Mostra i secondi"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr "Notifiche:"

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr "Mostrare testo delle notifiche:"

#: package/contents/ui/configTimes.qml:76
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""
"Se aggiungi qui dei timer predefiniti, appariranno nel menu contestuale del "
"plasmoide."

#: package/contents/ui/configTimes.qml:83
#, kde-format
msgid "Add"
msgstr "Aggiungi"

#: package/contents/ui/configTimes.qml:120
#, kde-format
msgid "Scroll over digits to change time"
msgstr "Scorri sulle cifre per modificare l'ora"

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr "Applica"

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr "Annulla"

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr "Modifica"

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr "Elimina"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "%1 is running"
msgstr "%1 è in esecuzione"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "%1 not running"
msgstr "%1 non è in esecuzione"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] "Tempo rimanente: %1 secondo"
msgstr[1] "Tempo rimanente: %1 secondi"

#: package/contents/ui/main.qml:62
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""
"Usa la rotella del mouse per cambiare le cifre o scegliere dai timer "
"predefiniti nel menu contestuale"

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Timer"
msgstr "Timer"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Timer finished"
msgstr "Timer terminato"

#: package/contents/ui/main.qml:137
#, kde-format
msgctxt "@action"
msgid "&Start"
msgstr "&Avvia"

#: package/contents/ui/main.qml:138
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr "&Ferma"

#: package/contents/ui/main.qml:139
#, kde-format
msgctxt "@action"
msgid "&Reset"
msgstr "Azze&ra"

#~ msgid "Timer is running"
#~ msgstr "Il timer è in esecuzione"

#~ msgctxt "@title:group"
#~ msgid "Run Command"
#~ msgstr "Esegui un comando"

#~ msgctxt "@label:textbox"
#~ msgid "Command:"
#~ msgstr "Comando:"

#~ msgctxt "@label:textbox"
#~ msgid "Title:"
#~ msgstr "Titolo:"

#~ msgctxt "@label:textbox"
#~ msgid "Text:"
#~ msgstr "Testo:"

#~ msgctxt "separator of hours:minutes:seconds in timer strings"
#~ msgid ":"
#~ msgstr ":"

#~ msgid "Timer Configuration"
#~ msgstr "Configurazione timer"

#~ msgid "Stop"
#~ msgstr "Ferma"

#~ msgid "Timer Timeout"
#~ msgstr "Scadenza del timer"

#~ msgid "General"
#~ msgstr "Generale"

#~ msgid "Plasma Timer Applet"
#~ msgstr "Conto alla rovescia di Plasma"

#~ msgid "Actions on Timeout"
#~ msgstr "Azioni alla scadenza"

#~ msgid "Show a message:"
#~ msgstr "Mostra un messaggio:"

#~ msgid "Message:"
#~ msgstr "Messaggio:"
