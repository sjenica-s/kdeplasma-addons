# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Tamas Szanto <tszanto@interware.hu>, 2009.
# Kiszel Kristóf <ulysses@kubuntu.org>, 2010, 2017.
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4.3\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-23 00:46+0000\n"
"PO-Revision-Date: 2021-01-02 14:41+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.03.70\n"

#: spellcheck.cpp:98 spellcheck_config.cpp:65 spellcheck_config.cpp:96
#, kde-format
msgid "spell"
msgstr "helyesírás"

#: spellcheck.cpp:103
#, kde-format
msgctxt ""
"Spelling checking runner syntax, first word is trigger word, e.g.  \"spell\"."
msgid "%1:q:"
msgstr "%1:q:"

#: spellcheck.cpp:104
#, kde-format
msgid "Checks the spelling of :q:."
msgstr "Ellenőrzi a következő helyesírását: :q:."

#: spellcheck.cpp:224
#, kde-format
msgctxt "Term is spelled correctly"
msgid "Correct"
msgstr "Helyes"

#: spellcheck.cpp:233
#, kde-format
msgid "Suggested term"
msgstr "Javaslatok"

#: spellcheck.cpp:263
#, kde-format
msgid "No dictionary found, please install hspell"
msgstr "Nem található szótár, telepítse a hspell-t"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: spellcheck_config.ui:17
#, kde-format
msgid "Spell Check Settings"
msgstr "Helyesírás-ellenőrzés beállításai"

#. i18n: ectx: property (text), widget (QCheckBox, m_requireTriggerWord)
#: spellcheck_config.ui:23
#, kde-format
msgid "&Require trigger word"
msgstr "K&ulcsszó szükséges"

#. i18n: ectx: property (text), widget (QLabel, label)
#: spellcheck_config.ui:32
#, kde-format
msgid "&Trigger word:"
msgstr "&Kulcsszó:"

#. i18n: ectx: property (text), widget (QPushButton, m_openKcmButton)
#: spellcheck_config.ui:62
#, fuzzy, kde-format
#| msgid "Configure Dictionaries..."
msgid "Configure Dictionaries…"
msgstr "Szótárak beállítása…"

#~ msgctxt "@action"
#~ msgid "Copy to Clipboard"
#~ msgstr "Másolás a vágólapra"

#~ msgid "Could not find a dictionary."
#~ msgstr "Nem található szótár."

#~ msgctxt "separator for a list of words"
#~ msgid ", "
#~ msgstr ", "
