# Translation of plasma_applet_calculator into esperanto.
#
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_calculator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:50+0000\n"
"PO-Revision-Date: 2009-12-19 23:37+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: Esperanto <kde-i18n-doc@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/calculator.qml:285
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr ""

#: package/contents/ui/calculator.qml:308
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:321
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr ""

#: package/contents/ui/calculator.qml:334
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:346
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:398
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/calculator.qml:450
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:501
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="

#~ msgctxt "The − button of the calculator"
#~ msgid "−"
#~ msgstr "−"

#~ msgid "ERROR"
#~ msgstr "ERARO"

#~ msgid "ERROR: DIV BY 0"
#~ msgstr "ERARO: DIV PER 0"

#~ msgid "Copy"
#~ msgstr "Kopii"

#~ msgid "Paste"
#~ msgstr "Alglui"

#~ msgctxt "The ∕ button of the calculator"
#~ msgid "∕"
#~ msgstr "∕"
