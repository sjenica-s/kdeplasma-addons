# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# giovanni <g.sora@tiscali.it>, 2017, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-01 00:49+0000\n"
"PO-Revision-Date: 2021-05-16 23:07+0100\n"
"Last-Translator: Giovanni Sora <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Apparentia"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Predefined Timers"
msgstr "Chronometros temporisator predefinite"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr "Avantiate"

#: package/contents/ui/CompactRepresentation.qml:133
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "Temporisator"

#: package/contents/ui/CompactRepresentation.qml:133
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Start Timer"
msgstr "Temporisator"

#: package/contents/ui/CompactRepresentation.qml:189
#: package/contents/ui/CompactRepresentation.qml:206
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr "Postea que temporisator completa:"

#: package/contents/ui/configAdvanced.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Execute command:"
msgstr "Executa commando:"

#: package/contents/ui/configAppearance.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr "Monstrator:"

#: package/contents/ui/configAppearance.qml:33
#, kde-format
msgctxt "@option:check"
msgid "Show title:"
msgstr "Monstra titulo:"

#: package/contents/ui/configAppearance.qml:50
#, kde-format
msgctxt "@option:check"
msgid "Show seconds"
msgstr "Monstra secundas"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr "Notificationes:"

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr "Monstra etxto de notification:"

#: package/contents/ui/configTimes.qml:76
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""
"Si tu adde temporisatores predefinite hic, illos apparera inmenu de contexto "
"de plasmoid."

#: package/contents/ui/configTimes.qml:83
#, kde-format
msgid "Add"
msgstr "Adde"

#: package/contents/ui/configTimes.qml:120
#, kde-format
msgid "Scroll over digits to change time"
msgstr "Rola sur digitos per cambiar tempore"

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr "Applica"

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr "Cancella"

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr "Modifica"

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr "Dele"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "%1 is running"
msgstr "%1 es executante"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "%1 not running"
msgstr "%1 non es in execution"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] "Tempore lassate remanente. %1 secunda"
msgstr[1] "Tempore remanente lassate: %1 secundas secundas"

#: package/contents/ui/main.qml:62
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""
"Usa rota de mus per cambiar digitos o selige ab temporisatores predefinite "
"in le menu de contexto"

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Timer"
msgstr "Temporisator"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Timer finished"
msgstr "Chronometro terminava"

#: package/contents/ui/main.qml:137
#, kde-format
msgctxt "@action"
msgid "&Start"
msgstr "&Initia"

#: package/contents/ui/main.qml:138
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr "S&toppa"

#: package/contents/ui/main.qml:139
#, kde-format
msgctxt "@action"
msgid "&Reset"
msgstr "&Re-fixa"

#~ msgid "Timer is running"
#~ msgstr "Chronometro temporisator es executante"

#, fuzzy
#~| msgid "Run command"
#~ msgctxt "@title:group"
#~ msgid "Run Command"
#~ msgstr "Executa Commando"

#, fuzzy
#~| msgid "Command:"
#~ msgctxt "@label:textbox"
#~ msgid "Command:"
#~ msgstr "Commando:"

#, fuzzy
#~| msgid "Title:"
#~ msgctxt "@label:textbox"
#~ msgid "Title:"
#~ msgstr "Titulo:"

#, fuzzy
#~| msgid "Text:"
#~ msgctxt "@label:textbox"
#~ msgid "Text:"
#~ msgstr "Texto:"
