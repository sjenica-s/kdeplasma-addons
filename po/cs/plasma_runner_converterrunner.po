# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Lukáš Tinkl <ltinkl@redhat.com>, 2010.
# Vít Pelčák <vit@pelcak.org>, 2011.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: krunner_converterrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:47+0000\n"
"PO-Revision-Date: 2021-03-24 11:47+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.3\n"

#: converterrunner.cpp:33
#, kde-format
msgid ""
"Converts the value of :q: when :q: is made up of \"value unit [>, to, as, "
"in] unit\". You can use the Unit converter applet to find all available "
"units."
msgstr ""
"Převede hodnotu :q: když se :q: skládá z \"value unit [>, to, as, in] unit"
"\". Můžete použít applet Převodník jednotek pro nalezení všech dostupných "
"jednotek."

#: converterrunner.cpp:42
#, kde-format
msgctxt "list of words that can used as amount of 'unit1' [in|to|as] 'unit2'"
msgid "in;to;as"
msgstr "v;do;na"

#: converterrunner.cpp:50
#, kde-format
msgid "Copy unit and number"
msgstr "Zkopírovat jednotku a číslo"
